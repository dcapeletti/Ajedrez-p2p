# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti
    
    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Este archivo contiene a la clase Mediador y a sus distintas implementaciones.
Puede haber varios mediadores correlacionados por distintos patrones si es necesario.
'''
import wx
import os
from observador import Observador
from widget import *
from chatnetworking import ChatConnect
import rendezvous


class Mediador():
    
    def widgetModificado(self, control, evento):
        raise NotImplemented('Este método es abstracto. Debe sobrescribirlo.')
    

class DirectorView(Mediador, Observador):
    def __init__(self):
        pass
    
    def widgetModificado(self, control, evento):
        #Escribir la lógica del mediador...Hacer algo con el widget que lanzo el evento...
        print control
        #print evento
        objeto = evento.GetEventObject()
        '''
        nuevoMenu = wx.Menu()
        item = nuevoMenu.Append(-1, '&Nuevo archivo',
                'Restablece el nombre a localhost')
        control.Append(nuevoMenu, 'Nuevo')'''
        
        if type(objeto) == wx.Menu: #Compruebo el tipo del objeto
            #print "Evento de menu"
            if evento.GetId() == 1: #Por el número del Id, obtengo el origen del menu
                print "Se ha echo clic en el primer menu"
                #if evento.GetEventType() == wx.EVT_MENU:
                #    print "Se ha echo click"
                from widget import eventDict
                print eventDict[evento.GetEventType()]
                from GUINuevaPartida import DialogoJugar
                gui = DialogoJugar(None)
                gui.ShowModal()
                gui.Destroy()
            else:
                print "Otro menu"
                
    
    def actualizar(self, sujetoDato, eventoInteres):
        pass
            
            
    
    def inicializarGUI(self):
        """Inicializa la GUI"""
        self.app = App(redirect=False)
        
        frame = FrameSensor(parent=None, id=-1, titulo=u'Ajedrez', mediador=self)
        self.app.setFrame(frame)
        self.app.getFrame().Show()
        self.app.SetTopWindow(self.app.getFrame()) #Debe llamarse a este método
        self.rendezvous = rendezvous.Rendezvous(self.connected, self.chatDisplay, 
                                                self.lostConnection) 
        #self.panel_autenticacion = Autenticacion(self.app.getFrame(), self) #GUI de prueba, mejorarla!!!
        #self.panel_autenticacion.Show()        
        self.app.MainLoop()
            
            
    def conectar(self):        
        wx.BeginBusyCursor() #Cursor en estado procesando
        
        net = ChatConnect(self.adminvariables.getHostServer(), self.rendezvous.connected, 
                          self.rendezvous.display, self.rendezvous.lost)
        #net.addObserver(self)
        self.adminvariables.setNet(net)
        
        self.adminvariables.getNet().start()
        self.connected=True
        self.panel_autenticacion = Autenticacion(self.app.getFrame(), self) #GUI de prueba, mejorarla!!!
        self.panel_autenticacion.Show()
        self.app.getFrame().GetMenuBar().agregarTodosLosMenus() #Agrego el menú de tareas luego del loguin
        wx.EndBusyCursor()
        
    
    def lostConnection(self, msg):
        """"
        Hemos perdido nuestra conexión con el servidor
        Invocado via :func:`wx.CallAfter` en :mod:`rendezvous`.
        """
        #self.connectBtn.SetLabel('Conectar')
        #self._not_connected()
        #self.add_readWin('\n\n'+ msg)
        self.connected = False
        self.adminvariables.getNet().join()
        print msg
        self.app.getFrame().GetStatusBar().SetStatusText(msg, 0)
        imagen = wx.StaticBitmap(self.app.getFrame().GetStatusBar())
        if os.name == "posix": 
            imagen.SetBitmap(wx.Bitmap('images/LedOff.png'))
        else:
            imagen.SetBitmap(wx.Bitmap('images\LedOff.png')) #para sistemas Windows.
        self.app.getFrame().GetStatusBar().AddWidget(imagen, pos=1)
        
    def chatDisplay(self, msg):
        try:
            if msg.find("\n") != -1: #Si encuentro un \n del mensaje del server
                self.comando = msg[msg.find("\n"):].strip()
                print msg
                if msg.find("<servidorBD>") != -1: #Path del servidor
                    server_bd = msg[msg.find("<servidorBD>")+12:msg.find("</servidorBD>")]
                    self.adminvariables.setHostServerBD(server_bd)
                if msg.find("<nombreBD>") != -1: #Nombre de la BD
                    base_dato = msg[msg.find("<nombreBD>")+10:msg.find("</nombreBD>")]
                    self.adminvariables.setNombreBD(base_dato)
                if msg.find("<sensorProx>") != -1:
                    cm =  msg[msg.find("<sensorProx>")+12:msg.find("</sensorProx>")].strip()
                    #self.calcularDistancia(float(cm))
                    self.panel_autenticacion.getPanelSensorAlimento().setPos(self.calcularDistancia(float(cm)))
                    wx.EndBusyCursor()
                    #print "ddasdsa ", cm
                if msg.find("<sensorTapa>") != -1: #Si encuentro que el botón se ha apretado...
                    estado_puerta = msg[msg.find("<sensorTapa>")+12:msg.find("</sensorTapa>")].strip()
                    #print estado_puerta
                    fecha = self.adminvariables.getConexionBd().getUltimaAperturaPuertaTolva() 
                    fecha_y_hora = fecha.strftime('%d/%m/%Y') + " a las \n " + fecha.strftime('%H:%M:%S')
                    self.panel_autenticacion.getPanelSensorPuerta().cambiarEstado(int(estado_puerta), fecha_y_hora) 
                if msg.find("<botonCompuerta comp=") != -1: #Si encuentro la palabra compuerta
                    estado_compuerta = msg[msg.find(">")+1:msg.find("</")].strip()
                    id_compuerta = msg[msg.find("=")+1:msg.find(">")].strip() #identificador de compuerta.
                    self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(10*(1-int(estado_compuerta)), int(id_compuerta)) #Imprimo el valor en el slider...
                    self.panel_autenticacion.getPanelMotor().cambiarEstadoControlMaestro(3)#Lo pongo en estado bajo.
                    mensaje_estado = "Abierta." if int(estado_compuerta) == 0 else "Cerrada."
                    self.app.getFrame().GetStatusBar().SetStatusText("Evento en compuerta " + id_compuerta + ". Estado " + mensaje_estado, 0)
                if msg.find("pservo:") != -1: #Si encuentro la palabra pservo
                    estado_compuerta = msg[msg.find("o:")+2:msg.find("o:")+4].strip()
                    #self.panel_autenticacion.getPanelMotor().cambiarEstadoControlPuerta(int(estado_compuerta)) #Imprimo el valor en el slider...
                    self.panel_autenticacion.getPanelMotor().cambiarTodasLasCompuertas(int(estado_compuerta))
                if msg.find("Ejecutando tarea:") != -1:
                    id_tarea = int(msg.split(",")[0].split(":")[1])
                    id_compuerta = msg.split(",")[1]     
                    self.app.getFrame().GetStatusBar().SetStatusText("Ejecutando tarea " + str(id_tarea), 0)         
                #self.app.getFrame().GetStatusBar().SetStatusText(self.comando, 0) #Imprimo TODOS los mensajes del server en la barra de noticicaciones        
            #self.app.getFrame().GetStatusBar().SetStatusText(self.comando, 0)
        except ImportError as e:
            self.verificarError(e)
            
            
    def cerrarConexion(self):
        "Close the application by Destroying the object"
        if self.connected:
            self.adminvariables.getNet().send("/quit")
            self.adminvariables.getNet().join()

            
            
            
            
            
            
            