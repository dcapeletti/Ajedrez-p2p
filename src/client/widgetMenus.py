# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

import wx
from widget import Widget
#------------------------------------------------------------------------------------

class MenuBar(wx.MenuBar, Widget):
    def __init__(self, parent, mediador):
        wx.MenuBar.__init__(self)
        self.frame = parent #Es el Frame contenedor
        self.mediador = mediador
        
        self.mnuArchivo = wx.Menu() 
        #1, 2 son Id de los menues
        mnuNuevaPartida = self.mnuArchivo.Append(1, '&Nueva partida',
                'Restablece el nombre a localhost')
        mnuSalir = self.mnuArchivo.Append(2, '&Salir', 'Salir de la aplicación')
        
        self.frame.Bind(wx.EVT_MENU, self.prueba, mnuNuevaPartida)
        self.frame.Bind(wx.EVT_MENU, self.prueba, mnuSalir)
        self.Append(self.mnuArchivo, '&Archivo')
        
    
    def prueba(self, event):
        self.notificarCambios(event)
        print "Se ha echo click en el método prueba"
    
    
    def notificarCambios(self, evento):
        #Se tiene que enviar notificaciones del evento al mediador...
        self.mediador.widgetModificado(self, evento)
   
   
#------------------------------------------------------------------------------------
