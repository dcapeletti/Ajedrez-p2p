# coding=utf-8
# Thanks Tim Bower 
# Copyright 2009, Tim Bower. Apache Open Source License
# This program was developed for education purposes for the Network
# Programming Class, CMST 355, at Kansas State University at Salina.

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

"""
**File:** rendezvous.py

A couple functions in a class just to facilitate communication between the
networking thread and the main thread which does the graphics.  Basically, the
only purpose of these functions is so that the code that does the networking
does not have to know anything about how the graphics are done (wx, TK, ...).
Having this in a separate file also helps with imports so that there is not an
import loop between the graphics and networking.

When used with wxPython, We just use the :func:`wx.CallAfter` function to send
data from the networking thread to the wxFrame in the main thread.

If used with TKinter graphics (a previous implementation), we have to use a
queue and generate new events on our own -- much more complicated.
"""


import wx

class Rendezvous(object):
    """
    Messages from the networking thread get left here and
    the appropriate wxPython events are generated to notify and pass
    data to the graphics thread.
    """
    def __init__(self, wxConnected, wxDisplay, wxLost):
        self.wxConnected = wxConnected
        self.wxDisplay = wxDisplay
        self.wxLost = wxLost

    def connected(self):
        "Notify the main tread that we are connected to the server"
        wx.CallAfter(self.wxConnected)

    def display(self, msg):
        "shuttle a message to be displayed in the chat read window"
        wx.CallAfter(self.wxDisplay, msg)

    def lost(self, msg):
        "Notify the main tread that the network connection dropped"
        wx.CallAfter(self.wxLost, msg)