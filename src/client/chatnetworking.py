# coding=utf-8

# Thanks Tim Bower 
# Copyright 2009, Tim Bower. Apache Open Source License
# This program was developed for education purposes for the Network
# Programming Class, CMST 355, at Kansas State University at Salina.

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

"""
**File:** chatnetwork.py

The networking portion of chat room client.
Most of this runs in a separate thread from the main thread which manages
the graphical interface. See wxchat.py
"""

import socket
import threading

import rendezvous

defaulthost = 'localhost'
port = 50000

class ChatConnect(threading.Thread):
    """
    Run as a separate thread to make and manage the socket connection to the
    chat server.
    """
    def __init__(self, host, connected, display, lost):
        threading.Thread.__init__(self)
        self.host = host
        self.connected = connected
        self.display = display
        self.lost = lost
        self.msgLock = threading.Lock()
        self.numMsg = 0
        self.msg = []

    def run(self):
        "The new thread starts here to listen for data from the server"
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.socket.settimeout(1)
        try:
            self.socket.connect((self.host, port))
        except:
            self.lost("Imposible conectar con %s. Verifique el estado del servidor." % self.host)
            return
        self.connected()
        while True:
            self.__send()
            try:
                data = self.socket.recv(4096)
            # Timeout once in a while just to check user input
            except socket.timeout:
                continue
            except:  # server was stopped or had some error
                self.lost("Conexión de red cerrada por el servidor...")
                break
            if len(data):
                self.display(data)
            else:
                # no data when peer does a socket.close()
                self.lost("Conexión de red cerrada ...")
                break
        # End loop of network send / recv data
        self.socket.close()

    def __send(self):
        """
        Actually send a message, if one is available, to the server.
        Need to acquire lock for the message queue.
        """
        self.msgLock.acquire()
        if self.numMsg > 0:
            self.socket.send(self.msg.pop(0))
            self.numMsg -= 1
        self.msgLock.release()

    def send(self, msg):
        """
        Set up to send a message to the server - called from main thread
        This is the only part of this class that executes in the main tread,
        We use a list to drop off the message for the networking thread to pick
        up and actually send it.  We could use a Queue.Queue object, which
        comes standard with Python and not have to mess with locks.  When the
        graphics were done with Tkinter, I did that to send data back to
        the main thread.  This locking stuff is pretty simple, so it's a good
        place to see how to do the locking ourself.
        """
        self.msgLock.acquire()
        self.msg.append(msg)
        self.numMsg += 1
        self.msgLock.release()