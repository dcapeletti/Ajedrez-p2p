# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
En este módulo se deberá implementar el patrón MEDIADOR entre los objetos que forman
parte de la vista (Interfaz gráfica). Entre estos objetos pueden estar los menúes, botoneras,
etc.
'''
import wx
from mediador import DirectorView
from threadClientTuntox import ClientTuntox

xmax = 500
ymax = 500
MAIN_WINDOW_DEFAULT_SIZE = (xmax,ymax)

eventDict = eventDict = {}

class Widget():
    
    def notificarCambios(self, evento):
        raise NotImplemented('Este método es abstracto. Debe sobrescribirlo.')

#-----------------------------------------------------------------------------------    
class App(wx.App):

    def OnInit(self):
        med = DirectorView()
        self.mediador=med
        
        self.__cargarEventos()
        
        self.frame = FrameSensor(parent=None, id=-1,
                titulo='Ajedrez', mediador=med)
        self.frame.Show()
        self.SetTopWindow(self.frame)
    
        return True
    
    def __cargarEventos(self):
        """
        Carga los eventos para luego buscar que evento se ha lanzado...    
        """
        for name in dir(wx):
            if name.startswith('EVT_'):
                evt = getattr(wx, name)
                if isinstance(evt, wx.PyEventBinder):
                    eventDict[evt.typeId] = name
    
#-----------------------------------------------------------------------------------    
class FrameSensor(wx.Frame, Widget):
    def __init__(self, parent, id, titulo, mediador):
        estilo = wx.DEFAULT_FRAME_STYLE
        self.mediador = mediador
        wx.Frame.__init__(self, parent, id, title=titulo, size=MAIN_WINDOW_DEFAULT_SIZE, style=estilo)
        self.Center()
        wx.BeginBusyCursor() #http://xoomer.virgilio.it/infinity77/wxPython/wxFunctions.html#BeginBusyCursor
        self.panel = wx.Panel(self)
        self.Bind(wx.EVT_CLOSE, self.OnExit) #Paso el método/función definido para el evento CLOSE
        
        from widgetMenus import MenuBar 
        self.menuBar = MenuBar(self, self.mediador) #Es el barra de menú
        self.SetMenuBar(self.menuBar)
        
        self.barraEstado = BarraEstado(self) #Es la barra de estado
        self.SetStatusBar(self.barraEstado)
        
    
    def OnExit(self, event):
        "Close the application by Destroying the object"
        '''if self.connected:
            self.net.send("/quit")
            self.net.join()'''
        #self.notificarCambios(event)
        self.Destroy()
        
    
    def notificarCambios(self, evento):
        #Se tiene que enviar notificaciones del evento al mediador...
        self.mediador.widgetModificado(self, evento)
        #print evento
        
        
class Autenticacion(wx.Panel, Widget):
    def __init__( self, parent, mediador):
        #wx.Panel.__init__ ( self, parent, id = wx.ID_ANY, pos = wx.DefaultPosition, size = wx.Size( xmax,ymax ), style = wx.TAB_TRAVERSAL | wx.ALIGN_CENTER)
        wx.Panel.__init__(self,parent)
        self.mediador = mediador
        #self.SetBackgroundColour(wx.RED)
        
        main_box_sizer = wx.BoxSizer( wx.HORIZONTAL) #Sizer principal. Agrupa la distribución principal de componentes
        
        #self.panel_sensor_distancia = SensorNivelAlimento(parent, mediador)
        
        #self.panel_sensor_puertas = SensorPuertas(parent, mediador)      
        
        #self.panel_control_motor = PanelSensorCompuerta(parent, mediador)
        
       
        #main_box_sizer.Add(self.panel_sensor_distancia,0,wx.ALL|wx.EXPAND,5)
        #main_box_sizer.Add(self.panel_sensor_puertas,0,wx.ALL,5)
        #main_box_sizer.Add(self.panel_control_motor,1,wx.ALL|wx.EXPAND,5)
                
        #self.SetSizer(main_box_sizer)
        main_box_sizer.Fit(self)
        parent.SetSizeWH(700,350) #Luego de crear todos los componentes, ajusto el tamaño.
        parent.CenterOnScreen() #Centro en la pantalla.
        self.Layout()   
        
        
    def __del__( self ):
        pass
    
    def notificarCambios(self, evento):
        self.mediador.widgetModificado(self, evento)
        
    def getPanelSensorAlimento(self):
        return self.panel_sensor_distancia
    
    def getPanelSensorPuerta(self):
        return self.panel_sensor_puertas
    
    def getPanelMotor(self):
        return self.panel_control_motor
    
    
#----------------------------------------------------------------------------------------
class BarraEstado(wx.StatusBar):
    def __init__(self, parent):
        wx.StatusBar.__init__(self, parent)
        self.SetFieldsCount(2)
    
        
    
