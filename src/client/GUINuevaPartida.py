#coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

import wx

class DialogoJugar(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__ ( self, parent, id = wx.ID_ANY, title = 'Nuevo juego', pos = wx.DefaultPosition, style = wx.DEFAULT_DIALOG_STYLE )
        self.configurarGUI()
        
    def configurarGUI(self):
        self.panelPrincipal = wx.Panel(self)
        self.optInvitarOponente = wx.RadioButton(self.panelPrincipal, 1, "Invitar a un oponente")
        self.optInvitarOponente.Bind(wx.EVT_RADIOBUTTON, self.mostrarPanelCodigo)
        self.optUnirmeAJugada = wx.RadioButton(self.panelPrincipal, 2, "Unirme a una jugada")
        self.optUnirmeAJugada.Bind(wx.EVT_RADIOBUTTON, self.mostrarPanelCodigo)
        btnAceptar= wx.Button(self.panelPrincipal, -1, "Aceptar")
        btnAceptar.Bind(wx.EVT_BUTTON, self.procesarOpcion)
        btnCancelar= wx.Button(self.panelPrincipal, wx.ID_CANCEL, "Cancelar")
        #btnCancelar.Bind(wx.EVT_BUTTON, self.cerrarDialogo)
        
        self.panelCodigInvitado = wx.Panel(self.panelPrincipal)
        lblCodigoInvitado = wx.StaticText(self.panelCodigInvitado, -1, "Ingrese el código de invitado:")
        self.txtCodigoInvitado = wx.TextCtrl(self.panelCodigInvitado, -1, size=(520, -1))
        
        sizerCodigoInvitado = wx.BoxSizer(wx.HORIZONTAL)
        sizerCodigoInvitado.Add(lblCodigoInvitado, flag=wx.ALIGN_CENTER_VERTICAL)
        sizerCodigoInvitado.Add(self.txtCodigoInvitado)
        self.panelCodigInvitado.SetSizer(sizerCodigoInvitado)
        self.panelCodigInvitado.Hide()
        
        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(self.optInvitarOponente)
        self.mainSizer.Add(self.optUnirmeAJugada)
        
        sizerBotones = wx.BoxSizer(wx.HORIZONTAL)
        sizerBotones.Add(btnCancelar)
        sizerBotones.Add(btnAceptar)
        
        self.mainSizer.Add(self.panelCodigInvitado)
        self.mainSizer.Add(sizerBotones, 1, flag=wx.ALIGN_CENTER_HORIZONTAL)
        
        self.panelPrincipal.SetSizer(self.mainSizer)
        
        self.mainSizer.Fit(self)
        self.Center()
        
        
    def mostrarPanelCodigo(self, event):
        if event.GetEventObject().GetId()==2:
            self.panelCodigInvitado.Show(True)
            self.panelPrincipal.SetSizerAndFit(self.mainSizer)
            self.mainSizer.Fit(self)
            self.panelPrincipal.Layout()
            self.Layout()
            #self.mainSizer.Fit(self)
            self.Center()
        else:
            self.panelCodigInvitado.Hide()
            self.panelPrincipal.SetSizerAndFit(self.mainSizer)
            self.mainSizer.Fit(self)
            self.panelPrincipal.Layout()
            self.Layout()
            #self.mainSizer.Fit(self)
            self.Center()
        
        
    def cerrarDialogo(self, event):
        self.Destroy()
        
        
    def procesarOpcion(self, event):
        print "procesando..."
        if self.optUnirmeAJugada.GetValue() == 1:
            if len(self.txtCodigoInvitado.GetValue()) < 81: #validar ir tox + puerto
                print "El cógido es incorrecto..."
                dialog = wx.MessageDialog(self, 'Valor incorrecto. El código debe tener 81 digitos.',' Código incorrecto', 
                        wx.OK | wx.ICON_ERROR)
    
                dialog.ShowModal()
                dialog.Destroy()
                return
            else:
                print "Iniciando tuntox modo cliente" #Funciona ok
                from threadClientTuntox import ClientTuntox
                dlg = wx.TextEntryDialog(self,
                    "Ingrese la contraseña de seguridad.",
                    "Contraseña de seguridad", style=wx.OK | wx.CANCEL )
                if dlg.ShowModal() == wx.ID_OK:
                    self.contrasena = dlg.GetValue()
                dlg.Destroy()
                print "Id tox", self.txtCodigoInvitado.GetValue()[0:76]
                print "Puerto reenvío", self.txtCodigoInvitado.GetValue()[76:]
                s = ClientTuntox(self.contrasena, self.txtCodigoInvitado.GetValue()[0:76], self.txtCodigoInvitado.GetValue()[76:])
                s.start()
        else: #La opción de invitar a otro es seleccionada
            print "Iniciando tuntox modo servidor" #hacer un thread para este hilo
            dlg = wx.TextEntryDialog(self,
                    "Para mas seguridad, ingrese una contraseña que deberá compartir únicamente con su oponente.",
                    "Contraseña de seguridad", style=wx.OK | wx.CANCEL )
            if dlg.ShowModal() == wx.ID_OK:
                self.contrasena = dlg.GetValue()
            dlg.Destroy()
            import sys
            sys.path.append("../")
            from server import server
            server.iniciarServer(self.contrasena)
            import time 
            time.sleep(2)
            dialog = wx.MessageDialog(self, 'Envíe este código de invitación a su oponente: ' + server.getIdTuntoxServer(),'Código de invitación', 
                        wx.OK | wx.ICON_INFORMATION)
    
            dialog.ShowModal()
            dialog.Destroy()
            
        self.Destroy()
        


#app = wx.App()
#dd = DialogoJugar(None)
#dd.ShowModal()
#dd.Destroy()
#app.MainLoop()