# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''


class Observador():
    def actualizar(self, sujetoDato, eventoInteres):
        raise NotImplemented("Debe implementar este método.")
    

class SujetoDato(object):
    def __init__(self):
        self._observadores = [] #Tupla o lista de observadores. Se podría usar conjuntos (set) para no repetir observadores
    
    def addObserver(self, observador):
        self._observadores.append(observador)
        
    def removeObserver(self, observador):
        self._observadores.remove(observador)
        
    def notificarCambios(self, eventoInteres):
        for obs in self._observadores:
            obs.actualizar(self, eventoInteres)