# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Thanks julia
'''
'''
Main application entry point
Note: If you execute a command that needs user_input and has
      a long output it may hang, try to avoid commands with input when possible
'''

from subprocess import Popen, PIPE, STDOUT
import platform
import os
from process_executor import execute as execute_process


def execute(cmd, user_input=None, env=None):
    '''
    Execute external process
    Params:
        user_input: string, ending in '\n'
                    e.g. 'name\npwd\n'
        env: dict containint environment
    '''
    user_input = user_input or []
    if env is None:
        env = os.environ

    if not user_input:
        _, out = execute_process(cmd, env=env)
    elif platform.system() != 'Windows':
        out = execute_unix(cmd, user_input, env=env)
    else:
        out = execute_windows(cmd, user_input, env=env)
        
    return out


def execute_unix(cmd, user_input, env):
    proc = Popen(cmd, shell=True, bufsize=1, stdout=PIPE,
                 stdin=PIPE, stderr=STDOUT, env=env)
    if user_input:
        proc.stdin.write(user_input)
    output = []

    while True:
        line = proc.stdout.readline()
        if not line:
            break
        output.append(line)
        print line
    out, err = proc.communicate()
    if out:
        output.append(out)
    err = err or ""
    output = ''.join(output)
    out = output + '\n' + err
    print out
    return out


def execute_windows(cmd, user_input, env):
    proc = Popen(cmd, shell=True, bufsize=1, stdout=PIPE,
                 stdin=PIPE, stderr=STDOUT, env=env)
    out, err = proc.communicate(input=user_input)
    err = err or ""
    print out
    print err
    return out + '\n' + err
