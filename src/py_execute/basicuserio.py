# coding=utf-8

'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

'''
Thanks julia
'''
import sys
from outputstream_wrapper import OutputStreamWrapper


class BasicUserIO(object):
    """Class to bypass output and input"""

    def __init__(self, ins=sys.stdin, out=None):
        '''
        Params:
            ins: input stream
            out: output stream, should have write method
        '''
        self._ins = ins
        if not out:
            out = OutputStreamWrapper(sys.stdout)
        self._out = out

    @property
    def out(self):
        """Get output stream"""
        return self._out

    @property
    def ins(self):
        """Get input stream"""
        return self._ins
