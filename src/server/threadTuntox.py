# coding=utf-8
'''
    Copyright © 2017 Diego Ariel Capeletti

    This file is part of Ajedrez-p2p, a game of chess on lan or p2p using 
    tuntox.

    Ajedrez-p2p is libre software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Ajedrez-p2p is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Ajedrez-p2p.  If not, see <http://www.gnu.org/licenses/>.
'''

import threading
import time
#import sys
#sys.path.append(sys.path[0]+'../py_execute/')
#print sys.path[-1]
from py_execute import run_command



class ServerTuntox(threading.Thread):
    def __init__(self, password):
        threading.Thread.__init__(self)
        self.daemon = True  #permite salir aunque el main se este ejecutando...
        self.paused = True  # Comienzo en pausa
        self.state = threading.Condition() #Devuelve un objeto de condición variable. Permite que uno o mas hilos esperen hasta que sean notificados...
        self.password = password

    def run(self):
        self.resume() #Al llamar a start(), el hilo se pone en marcha
        while True:
            with self.state:
                if self.paused: #Se evalua el estado del hilo, si esta pausado, lo pongo en espera...
                    self.state.wait() # block until notified #bloqeuar hasta que se notifica
            #hacer cosas
            #inicio tuntox y no hago nada mas... 
            try:     
                import sys
                print sys.path[0]     
                ret = run_command.execute("../tuntox/./tuntox-x64 -s '"+self.password+"'")
                time.sleep(0.1) #Tiempo de retaro del hilo
                self.pause() #Este hilo no se ejecuta mas...   
            except Exception as err:
                print "Error en threadTuntox:", err
                self.pause()


    def resume(self):
        #Pone en marcha la ejecución del thread
        with self.state:
            self.paused = False
            self.state.notify()  # unblock self if waiting #desbloquear auto esperando

    def pause(self):
        #Para la ejecución del thread
        with self.state:
            self.paused = True  # make self block and wait #El hilo deja de ejecutarse, entra en estado de espera..

    def detener_y_borrar(self):
        #Detiene el servo y limpiamos la configuración de pines GPIO
        self.pause()
        
    def getVal(self):
        from py_execute import process_executor
        return process_executor.datos
        
    

        